﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddingValues5seconds : MonoBehaviour {

    private string loginURL = "http://localhost/api/points/";
    private string getvalURL = "http://localhost/api/value/";
    private string putvalURL = "http://localhost/api/value/add";
    private ListOfPoints points;
    private float time = 0.0f;
    private float time2 = 0.0f;
    public float pausePeriod = 2.5f;
    public float pauseForRead = 4f;

    // Use this for initialization
    void Start () {
        StartCoroutine(Read());
    }

    IEnumerator Read()
    {
        points = new ListOfPoints();
        List<Point> list_points;

        WWW www = new WWW(loginURL);
        yield return www;

        string json_text = "{\"points\":" + www.text + "}";
        Wrapper lll = JsonUtility.FromJson<Wrapper>(json_text);
        list_points = lll.points;
        
        foreach (Point p in list_points)
        {
            points.points.Add(p);
        }

     }

    void Prepare()
    {
        int count = points.points.Count;

        System.Random r = new System.Random();

        float sample = (float)Math.Round(r.NextDouble(), 5);
        int range = r.Next(-10, 10);
        float f = sample + range;
        float ff = (float)Math.Round(f, 5);

        int randID = r.Next(0, count*5);
        randID = randID % count;
        int rId = points.points[randID].Id;
        WriteToDB(rId, ff);
    }

    void WriteToDB(int idP, float val)
    {
        WWWForm wwwf = new WWWForm();

        wwwf.AddField("Val", val.ToString());
        wwwf.AddField("PointID", idP.ToString());

        WWW www = new WWW(putvalURL, wwwf);
  //      Debug.Log("Upisano " + idP + ", " + val);
    }

    // Update is called once per frame
    void Update () {
        time += Time.deltaTime;
        time2 += Time.deltaTime;
        
        if(time>=pausePeriod)
        {
            time = 0.0f;

            Prepare();
        }

        if (time2 >= pauseForRead)
        {
            time2 = 0.0f;
            StartCoroutine(Read());
        }
    }
}
