﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour {
    
    public GameObject target;
    private float speed = 2.0f;
    private Vector3 point;
    private float angle = 30.0f;

    private float zoomSpeed = 150.0f;

    public float minX = -360.0f;
    public float maxX = 360.0f;

    public float minY = -45.0f;
    public float maxY = 45.0f;

    public float sensX = 100.0f;
    public float sensY = 100.0f;

    private int i = 0;

    float rotationY = 0.0f;
    float rotationX = 0.0f;


    void Start()
    {
        point = target.transform.position;
        transform.LookAt(point);
    }

    void Update()
    {
        float angleYtilt = Input.GetAxis("Horizontal") * angle;
        float angleZtilt = Input.GetAxis("Vertical") * angle;

        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
        {
            transform.RotateAround(point, new Vector3(0.0f, angleYtilt, angleZtilt), 20 * Time.deltaTime * speed);
        }

        if(Input.GetKey(KeyCode.UpArrow))
        {
            Vector3 v = transform.position;
            v.y += 10f;
            transform.position = v;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            Vector3 v = transform.position;
            v.y -= 10f;
            transform.position = v;
        }

        if (Input.GetMouseButton(1))
        {
            transform.localEulerAngles = new Vector3(135, 30, 175);
            rotationX += Input.GetAxis("Mouse X") * sensX * Time.deltaTime;
            rotationY += Input.GetAxis("Mouse Y") * sensY * Time.deltaTime;
            rotationY = Mathf.Clamp(rotationY, minY, maxY);
            transform.localEulerAngles = new Vector3(-(rotationY+225), rotationX + 40, 180);
        }

        if(Input.GetKey(KeyCode.Z))
        {
            Vector3 v = transform.position;
            v.x -= 5f;
            transform.position = v;
        }
        if (Input.GetKey(KeyCode.X))
        {
            Vector3 v = transform.position;
            v.x += 5f;
            transform.position = v;
        }
        if (Input.GetKey(KeyCode.C))
        {
            Vector3 v = transform.position;
            v.x -= 5f;
            transform.position = v;
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");
        transform.Translate(0, 0, scroll * zoomSpeed);
    }
}
