﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;


public class WriteJSON : MonoBehaviour {

    public void WritePoint(ListOfPoints p)
    {
  //      string url = "http://127.0.0.1/Resources/ListaTacaka.json";
        string json_list = "";
        Wrapper w = new Wrapper();
        w.points = p.points;

        Debug.Log(w.points.Count);

        json_list = JsonMapper.ToJson(w);


        Debug.Log(json_list);
   //     File.WriteAllText("C:\\wamp\\www\\Resources\\ListaTacaka.json", json_list);
        json_list = "";

    }


    public static List<Point> ReturnFromJSON(string jsonString)
    {
        List<Point> list = new List<Point>();
        
        list = LoadResourceTextfile();

        return list;
    }

    public static List<Point> LoadResourceTextfile()
    {
        TextAsset jsonObj = Resources.Load("ListaTacaka") as TextAsset;

        if (jsonObj == null)
            return null;


        var list = JsonUtility.FromJson<Wrapper>(jsonObj.text);

        return list.points;
    }

    public static List<Point> ListFromJson(string json)
    {
        var list = JsonUtility.FromJson<Wrapper>(json);

        return list.points;
    }
}



